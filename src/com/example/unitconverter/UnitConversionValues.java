package com.example.unitconverter;

import java.util.HashMap;

public class UnitConversionValues {
	public static final HashMap<String, Double> unitValuesMap = new HashMap<String, Double>() {
		{
			/******************** setting value for angle ************************/

			put("deg2rad", Double.valueOf(0.0174532925));
			put("deg2mil", Double.valueOf(17.777777778));
			put("deg2grad", Double.valueOf(1.1111111111));
			put("deg2deg", Double.valueOf(1));

			put("mil2rad", Double.valueOf(0.00098174770425));
			put("mil2mil", Double.valueOf(1));
			put("mil2grad", Double.valueOf(0.062));
			put("mil2deg", Double.valueOf(0.05625));

			put("rad2rad", Double.valueOf(1));
			put("rad2mil", Double.valueOf(1018.5916358));
			put("rad2grad", Double.valueOf(63.661977237));
			put("rad2deg", Double.valueOf(57.295779513));

			put("grad2rad", Double.valueOf(0.015707963268));
			put("grad2mil", Double.valueOf(16));
			put("grad2grad", Double.valueOf(1));
			put("grad2deg", Double.valueOf(0.9));

			/******************** setting value for area **********************************/

			put("acr2acr", Double.valueOf(1));
			put("acr2hec", Double.valueOf(0.40468564224));
			put("acr2squ", Double.valueOf(435.6));
			put("acr2sqf", Double.valueOf(43560));
			put("acr2sqi", Double.valueOf(6272640));
			put("acr2sqk", Double.valueOf(0.0040468564224));
			put("acr2sqm", Double.valueOf(4046.8564224));
			put("acr2sqml", Double.valueOf(0.0015625));

			put("hec2acr", Double.valueOf(2.4710538147));
			put("hec2hec", Double.valueOf(1));
			put("hec2squ", Double.valueOf(1076.3910417));
			put("hec2sqf", Double.valueOf(107639.10417));
			put("hec2sqi", Double.valueOf(15500031));
			put("hec2sqk", Double.valueOf(0.01));
			put("hec2sqm", Double.valueOf(10000));
			put("hec2sqml", Double.valueOf(0.0038610215855));

			put("squ2acr", Double.valueOf(0.0022956841139));
			put("squ2hec", Double.valueOf(0.0009290304));
			put("squ2squ", Double.valueOf(1));
			put("squ2sqf", Double.valueOf(100));
			put("squ2sqi", Double.valueOf(14400));
			put("squ2sqk", Double.valueOf(0.000009290304));
			put("squ2sqm", Double.valueOf(9.290304));
			put("squ2sqml", Double.valueOf(0.000003587006428));

			put("sqf2acr", Double.valueOf(0.000022956841139));
			put("sqf2hec", Double.valueOf(0.000009290304));
			put("sqf2squ", Double.valueOf(0.01));
			put("sqf2sqf", Double.valueOf(1));
			put("sqf2sqi", Double.valueOf(144));
			put("sqf2sqk", Double.valueOf(9.290304e-8));
			put("sqf2sqm", Double.valueOf(0.09290304));
			put("sqf2sqml", Double.valueOf(3.587006428e-8));

			put("sqi2acr", Double.valueOf(1.5942250791e-7));
			put("sqi2hec", Double.valueOf(6.4516e-8));
			put("sqi2squ", Double.valueOf(0.000069444444444));
			put("sqi2sqf", Double.valueOf(0.0069444444444));
			put("sqi2sqi", Double.valueOf(1));
			put("sqi2sqk", Double.valueOf(6.4516e-10));
			put("sqi2sqm", Double.valueOf(0.00064516));
			put("sqi2sqml", Double.valueOf(2.4909766861e-10));

			put("sqk2acr", Double.valueOf(247.10538147));
			put("sqk2hec", Double.valueOf(100));
			put("sqk2squ", Double.valueOf(107639.10417));
			put("sqk2sqf", Double.valueOf(10763910.417));
			put("sqk2sqi", Double.valueOf(1550003100));
			put("sqk2sqk", Double.valueOf(1));
			put("sqk2sqm", Double.valueOf(1000000));
			put("sqk2sqml", Double.valueOf(0.38610215855));

			put("sqm2acr", Double.valueOf(0.00024710538147));
			put("sqm2hec", Double.valueOf(0.0001));
			put("sqm2squ", Double.valueOf(0.10763910417));
			put("sqm2sqf", Double.valueOf(10.763910417));
			put("sqm2sqi", Double.valueOf(1550.0031));
			put("sqm2sqk", Double.valueOf(0.000001));
			put("sqm2sqm", Double.valueOf(1));
			put("sqm2sqml", Double.valueOf(3.8610215855e-7));

			put("sqml2acr", Double.valueOf(639.99999999));
			put("sqml2hec", Double.valueOf(258.99881103));
			put("sqml2squ", Double.valueOf(278784));
			put("sqml2sqf", Double.valueOf(27878400));
			put("sqml2sqi", Double.valueOf(4014489599.9));
			put("sqml2sqk", Double.valueOf(2.5899881103));
			put("sqml2sqm", Double.valueOf(2589988.1103));
			put("sqml2sqml", Double.valueOf(1));

			/********************** setting value for Bits and bytes *******************/

			put("bit2bit", Double.valueOf(1));
			put("bit2byt", Double.valueOf(0.125));
			put("bit2kbt", Double.valueOf(0.0001220703125));
			put("bit2mbt", Double.valueOf(1.1920928955e-7));
			put("bit2gbt", Double.valueOf(1.1641532183e-10));

			put("byt2bit", Double.valueOf(8));
			put("byt2byt", Double.valueOf(1));
			put("byt2kbt", Double.valueOf(0.0009765625));
			put("byt2mbt", Double.valueOf(9.5367431641e-7));
			put("byt2gbt", Double.valueOf(9.3132257462e-10));

			put("kbt2bit", Double.valueOf(8192));
			put("kbt2byt", Double.valueOf(1024));
			put("kbt2kbt", Double.valueOf(1));
			put("kbt2mbt", Double.valueOf(0.0009765625));
			put("kbt2gbt", Double.valueOf(9.5367431641e-7));

			put("mbt2bit", Double.valueOf(8388608));
			put("mbt2byt", Double.valueOf(1048576));
			put("mbt2kbt", Double.valueOf(1024));
			put("mbt2mbt", Double.valueOf(1));
			put("mbt2gbt", Double.valueOf(0.0009765625));

			put("gbt2bit", Double.valueOf(8.589934592E9));
			put("gbt2byt", Double.valueOf(1073741824));
			put("gbt2kbt", Double.valueOf(1048576));
			put("gbt2mbt", Double.valueOf(1024));
			put("gbt2gbt", Double.valueOf(1));

			/************************ setting value for Density **********************/

			put("gkl2gkl", Double.valueOf(1));
			put("gkl2glt", Double.valueOf(0.001));
			put("gkl2gmrl", Double.valueOf(1.0e-9));
			put("gkl2gml", Double.valueOf(0.000001));
			put("gkl2klt", Double.valueOf(0.000001));

			put("glt2gkl", Double.valueOf(1000));
			put("glt2glt", Double.valueOf(1));
			put("glt2gmrl", Double.valueOf(0.000001));
			put("glt2gml", Double.valueOf(0.001));
			put("glt2klt", Double.valueOf(0.001));

			put("gmrl2gkl", Double.valueOf(1000000000));
			put("gmrl2glt", Double.valueOf(1000000));
			put("gmrl2gmrl", Double.valueOf(1));
			put("gmrl2gml", Double.valueOf(1000));
			put("gmrl2klt", Double.valueOf(1000));

			put("gml2gkl", Double.valueOf(1000000));
			put("gml2glt", Double.valueOf(1000));
			put("gml2gmrl", Double.valueOf(0.001));
			put("gml2gml", Double.valueOf(1));
			put("gml2klt", Double.valueOf(1));

			put("klt2gkl", Double.valueOf(1000000));
			put("klt2glt", Double.valueOf(1000));
			put("klt2gmrl", Double.valueOf(0.001));
			put("klt2gml", Double.valueOf(1));
			put("klt2klt", Double.valueOf(1));

			/******************* setting value for electric current ********************/

			put("amp2amp", Double.valueOf(1));
			put("amp2gmp", Double.valueOf(1.0e-9));
			put("amp2kmp", Double.valueOf(0.001));
			put("amp2mmp", Double.valueOf(1000));
			put("amp2voh", Double.valueOf(1));
			put("amp2wvt", Double.valueOf(1));

			put("gmp2amp", Double.valueOf(1000000000));
			put("gmp2gmp", Double.valueOf(1));
			put("gmp2kmp", Double.valueOf(1000000));
			put("gmp2mmp", Double.valueOf(1.0E12));
			put("gmp2voh", Double.valueOf(1000000000));
			put("gmp2wvt", Double.valueOf(1000000000));

			put("kmp2amp", Double.valueOf(1000));
			put("kmp2gmp", Double.valueOf(0.000001));
			put("kmp2kmp", Double.valueOf(1));
			put("kmp2mmp", Double.valueOf(1000000));
			put("kmp2voh", Double.valueOf(1000));
			put("kmp2wvt", Double.valueOf(1000));

			put("mmp2amp", Double.valueOf(0.001));
			put("mmp2gmp", Double.valueOf(1.0e-12));
			put("mmp2kmp", Double.valueOf(0.000001));
			put("mmp2mmp", Double.valueOf(1));
			put("mmp2voh", Double.valueOf(0.001));
			put("mmp2wvt", Double.valueOf(0.001));

			put("voh2amp", Double.valueOf(1));
			put("voh2gmp", Double.valueOf(1.0e-9));
			put("voh2kmp", Double.valueOf(0.001));
			put("voh2mmp", Double.valueOf(1000));
			put("voh2voh", Double.valueOf(1));
			put("voh2wvt", Double.valueOf(1));

			put("wvt2amp", Double.valueOf(1));
			put("wvt2gmp", Double.valueOf(1.0e-9));
			put("wvt2kmp", Double.valueOf(0.001));
			put("wvt2mmp", Double.valueOf(1000));
			put("wvt2voh", Double.valueOf(1));
			put("wvt2wvt", Double.valueOf(1));

			/********************* setting value for energy ************************/

			put("cal2cal", Double.valueOf(1));
			put("cal2evt", Double.valueOf(2.613193933E19));
			put("cal2erg", Double.valueOf(41868000));
			put("cal2gjl", Double.valueOf(4.1868e-9));
			put("cal2gcl", Double.valueOf(1.000238903));
			put("cal2jul", Double.valueOf(4.1868));
			put("cal2kcl", Double.valueOf(0.001));
			put("cal2kjl", Double.valueOf(0.0041868));
			put("cal2whr", Double.valueOf(0.001163));

			put("evt2cal", Double.valueOf(3.8267347377e-20));
			put("evt2evt", Double.valueOf(1));
			put("evt2erg", Double.valueOf(1.6021773e-12));
			put("evt2gjl", Double.valueOf(1.6021773e-28));
			put("evt2gcl", Double.valueOf(3.827648956e-20));
			put("evt2jul", Double.valueOf(1.6021773e-19));
			put("evt2kcl", Double.valueOf(3.8267347377e-23));
			put("evt2kjl", Double.valueOf(1.6021773e-22));
			put("evt2whr", Double.valueOf(4.4504925e-23));

			put("erg2cal", Double.valueOf(2.3884589663e-8));
			put("erg2evt", Double.valueOf(6.24150648E11));
			put("erg2erg", Double.valueOf(1));
			put("erg2gjl", Double.valueOf(1.0e-16));
			put("erg2gcl", Double.valueOf(2.3890295762e-8));
			put("erg2jul", Double.valueOf(1.0e-7));
			put("erg2kcl", Double.valueOf(2.3884589663e-11));
			put("erg2kjl", Double.valueOf(1.0e-10));
			put("erg2whr", Double.valueOf(2.7777777778e-11));

			put("gjl2cal", Double.valueOf(238845896.63));
			put("gjl2evt", Double.valueOf(6.24150648e+27));
			put("gjl2erg", Double.valueOf(1.0E16));
			put("gjl2gjl", Double.valueOf(1));
			put("gjl2gcl", Double.valueOf(238902957.62));
			put("gjl2jul", Double.valueOf(1000000000));
			put("gjl2kcl", Double.valueOf(238845.89663));
			put("gjl2kjl", Double.valueOf(1000000));
			put("gjl2whr", Double.valueOf(277777.77778));

			put("gcl2cal", Double.valueOf(0.9997611541));
			put("gcl2evt", Double.valueOf(2.6125697824E19));
			put("gcl2erg", Double.valueOf(41858000));
			put("gcl2gjl", Double.valueOf(4.1858e-9));
			put("gcl2gcl", Double.valueOf(1));
			put("gcl2jul", Double.valueOf(4.1858));
			put("gcl2kcl", Double.valueOf(0.0009997611541));
			put("gcl2kjl", Double.valueOf(0.0041858));
			put("gcl2whr", Double.valueOf(0.0011627222222));

			put("jul2cal", Double.valueOf(0.23884589663));
			put("jul2evt", Double.valueOf(6.24150648E18));
			put("jul2erg", Double.valueOf(10000000));
			put("jul2gjl", Double.valueOf(1.0e-9));
			put("jul2gcl", Double.valueOf(0.23890295762));
			put("jul2jul", Double.valueOf(1));
			put("jul2kcl", Double.valueOf(0.00023884589663));
			put("jul2kjl", Double.valueOf(0.001));
			put("jul2whr", Double.valueOf(0.00027777777778));

			put("kcl2cal", Double.valueOf(1000));
			put("kcl2evt", Double.valueOf(2.613193933e+22));
			put("kcl2erg", Double.valueOf(4.1868E10));
			put("kcl2gjl", Double.valueOf(0.0000041868));
			put("kcl2gcl", Double.valueOf(1000.238903));
			put("kcl2jul", Double.valueOf(4186.8));
			put("kcl2kcl", Double.valueOf(1));
			put("kcl2kjl", Double.valueOf(4.1868));
			put("kcl2whr", Double.valueOf(1.163));

			put("kjl2cal", Double.valueOf(238.84589663));
			put("kjl2evt", Double.valueOf(6.24150648e+21));
			put("kjl2erg", Double.valueOf(1.0E10));
			put("kjl2gjl", Double.valueOf(0.000001));
			put("kjl2gcl", Double.valueOf(238.90295762));
			put("kjl2jul", Double.valueOf(1000));
			put("kjl2kcl", Double.valueOf(0.23884589663));
			put("kjl2kjl", Double.valueOf(1));
			put("kjl2whr", Double.valueOf(0.27777777778));

			put("whr2cal", Double.valueOf(859.84522786));
			put("whr2evt", Double.valueOf(2.2469423328e+22));
			put("whr2erg", Double.valueOf(3.6E10));
			put("whr2gjl", Double.valueOf(0.0000036));
			put("whr2gcl", Double.valueOf(860.05064743));
			put("whr2jul", Double.valueOf(3600));
			put("whr2kcl", Double.valueOf(0.85984522786));
			put("whr2kjl", Double.valueOf(3.6));
			put("whr2whr", Double.valueOf(1));

			/*************************** setting value for force ***********************/

			put("dyn2dyn", Double.valueOf(1));
			put("dyn2knt", Double.valueOf(1.0e-8));
			put("dyn2new", Double.valueOf(0.00001));

			put("knt2dyn", Double.valueOf(100000000));
			put("knt2knt", Double.valueOf(1));
			put("knt2new", Double.valueOf(1000));

			put("new2dyn", Double.valueOf(100000));
			put("new2knt", Double.valueOf(0.001));
			put("new2new", Double.valueOf(1));

			/******************* setting value for length **************************/

			put("cmt2cmt", Double.valueOf(1));
			put("cmt2fet", Double.valueOf(0.03280839895));
			put("cmt2inc", Double.valueOf(0.3937007874));
			put("cmt2kmt", Double.valueOf(0.00001));
			put("cmt2mtr", Double.valueOf(0.01));
			put("cmt2mil", Double.valueOf(0.0000062137119224));

			put("fet2cmt", Double.valueOf(30.48));
			put("fet2fet", Double.valueOf(1));
			put("fet2inc", Double.valueOf(12));
			put("fet2kmt", Double.valueOf(0.0003048));
			put("fet2mtr", Double.valueOf(0.3048));
			put("fet2mil", Double.valueOf(0.00018939393939));

			put("inc2cmt", Double.valueOf(2.54));
			put("inc2fet", Double.valueOf(0.083333333333));
			put("inc2inc", Double.valueOf(1));
			put("inc2kmt", Double.valueOf(0.0000254));
			put("inc2mtr", Double.valueOf(0.0254));
			put("inc2mil", Double.valueOf(0.000015782828283));

			put("kmt2cmt", Double.valueOf(100000));
			put("kmt2fet", Double.valueOf(3280.839895));
			put("kmt2inc", Double.valueOf(39370.07874));
			put("kmt2kmt", Double.valueOf(1));
			put("kmt2mtr", Double.valueOf(1000));
			put("kmt2mil", Double.valueOf(0.62137119224));

			put("mtr2cmt", Double.valueOf(100));
			put("mtr2fet", Double.valueOf(3.280839895));
			put("mtr2inc", Double.valueOf(39.37007874));
			put("mtr2kmt", Double.valueOf(0.001));
			put("mtr2mtr", Double.valueOf(1));
			put("mtr2mil", Double.valueOf(0.00062137119224));

			put("mil2cmt", Double.valueOf(160934.4));
			put("mil2fet", Double.valueOf(5280));
			put("mil2inc", Double.valueOf(63360));
			put("mil2kmt", Double.valueOf(1.609344));
			put("mil2mtr", Double.valueOf(1609.344));
			put("mil2mil", Double.valueOf(1));

			/*********************** setting value for Mass **********************/

			put("kgm2kgm", Double.valueOf(1));
			put("kgm2oun", Double.valueOf(35.27396195));
			put("kgm2pou", Double.valueOf(2.2046226218));
			put("kgm2ton", Double.valueOf(0.001));

			put("oun2kgm", Double.valueOf(0.028349523125));
			put("oun2oun", Double.valueOf(1));
			put("oun2pou", Double.valueOf(0.0625));
			put("oun2ton", Double.valueOf(0.000028349523125));

			put("pou2kgm", Double.valueOf(0.45359237));
			put("pou2oun", Double.valueOf(16));
			put("pou2pou", Double.valueOf(1));
			put("pou2ton", Double.valueOf(0.00045359237));

			put("ton2kgm", Double.valueOf(1000));
			put("ton2oun", Double.valueOf(35273.96195));
			put("ton2pou", Double.valueOf(2204.6226218));
			put("ton2ton", Double.valueOf(1));

			/*************************** setting value for power **************************/

			put("gwt2gwt", Double.valueOf(1));
			put("gwt2hwt", Double.valueOf(10000000));
			put("gwt2kwt", Double.valueOf(1000000));
			put("gwt2mwt", Double.valueOf(1000));
			put("gwt2wat", Double.valueOf(1000000000));

			put("hwt2gwt", Double.valueOf(1.0e-7));
			put("hwt2hwt", Double.valueOf(1));
			put("hwt2kwt", Double.valueOf(0.1));
			put("hwt2mwt", Double.valueOf(0.0001));
			put("hwt2wat", Double.valueOf(100));

			put("kwt2gwt", Double.valueOf(0.000001));
			put("kwt2hwt", Double.valueOf(10));
			put("kwt2kwt", Double.valueOf(1));
			put("kwt2mwt", Double.valueOf(0.001));
			put("kwt2wat", Double.valueOf(1000));

			put("mwt2gwt", Double.valueOf(0.001));
			put("mwt2hwt", Double.valueOf(10000));
			put("mwt2kwt", Double.valueOf(1000));
			put("mwt2mwt", Double.valueOf(1));
			put("mwt2wat", Double.valueOf(1000000));

			put("wat2gwt", Double.valueOf(1.0e-9));
			put("wat2hwt", Double.valueOf(0.01));
			put("wat2kwt", Double.valueOf(0.001));
			put("wat2mwt", Double.valueOf(0.000001));
			put("wat2wat", Double.valueOf(1));

			/************************** setting value for pressure ***********************/

			put("atm2atm", Double.valueOf(1));
			put("atm2gps", Double.valueOf(0.00010132501));
			put("atm2kps", Double.valueOf(101.32501));
			put("atm2mps", Double.valueOf(0.10132501));
			put("atm2pas", Double.valueOf(101325.01));

			put("gps2atm", Double.valueOf(9869.2316931));
			put("gps2gps", Double.valueOf(1));
			put("gps2kps", Double.valueOf(1000000));
			put("gps2mps", Double.valueOf(1000));
			put("gps2pas", Double.valueOf(1000000000));

			put("kps2atm", Double.valueOf(0.0098692316931));
			put("kps2gps", Double.valueOf(0.000001));
			put("kps2kps", Double.valueOf(1));
			put("kps2mps", Double.valueOf(0.001));
			put("kps2pas", Double.valueOf(1000));

			put("mps2atm", Double.valueOf(9.8692316931));
			put("mps2gps", Double.valueOf(0.001));
			put("mps2kps", Double.valueOf(1000));
			put("mps2mps", Double.valueOf(1));
			put("mps2pas", Double.valueOf(1000000));

			put("pas2atm", Double.valueOf(0.0000098692316931));
			put("pas2gps", Double.valueOf(1.0e-9));
			put("pas2kps", Double.valueOf(0.001));
			put("pas2mps", Double.valueOf(0.000001));
			put("pas2pas", Double.valueOf(1));

			/************************* setting value for speed **************************/

			put("mps2mps", Double.valueOf(1));
			put("mps2mls", Double.valueOf(0.00062137119224));
			put("mps2kph", Double.valueOf(3.6));
			put("mps2kps", Double.valueOf(0.001));

			put("mls2mps", Double.valueOf(1609.344));
			put("mls2mls", Double.valueOf(1));
			put("mls2kph", Double.valueOf(5793.6384));
			put("mls2kps", Double.valueOf(1.609344));

			put("kph2mps", Double.valueOf(0.27777777778));
			put("kph2mls", Double.valueOf(0.00017260310896));
			put("kph2kph", Double.valueOf(1));
			put("kph2kps", Double.valueOf(0.00027777777778));

			put("kps2mps", Double.valueOf(1000));
			put("kps2mls", Double.valueOf(0.62137119224));
			put("kps2kph", Double.valueOf(3600));
			put("kps2kps", Double.valueOf(1));

			/************************** setting value for temperature *************************/

			put("dcs2dcs", Double.valueOf(1));
			put("dcs2dfr", Double.valueOf(33.8));
			put("dcs2kvn", Double.valueOf(274.15));

			put("dfr2dcs", Double.valueOf(-17.2222222));
			put("dfr2dfr", Double.valueOf(1));
			put("dfr2kvn", Double.valueOf(255.9277778));

			put("kvn2dcs", Double.valueOf(-272.15));
			put("kvn2dfr", Double.valueOf(-457.87));
			put("kvn2kvn", Double.valueOf(1));

			/************************* setting value for time *****************************/

			put("mls2mls", Double.valueOf(1));
			put("mls2sec", Double.valueOf(0.001));
			put("mls2min", Double.valueOf(0.000016666666667));
			put("mls2hou", Double.valueOf(2.7777777778e-7));
			put("mls2day", Double.valueOf(1.1574074074e-8));
			put("mls2wek", Double.valueOf(1.6534391534e-9));
			put("mls2mon", Double.valueOf(3.8051750381e-10));
			put("mls2yar", Double.valueOf(3.1709791984e-11));

			put("sec2mls", Double.valueOf(1000));
			put("sec2sec", Double.valueOf(1));
			put("sec2min", Double.valueOf(0.016666666667));
			put("sec2hou", Double.valueOf(0.00027777777778));
			put("sec2day", Double.valueOf(0.000011574074074));
			put("sec2wek", Double.valueOf(0.0000016534391534));
			put("sec2mon", Double.valueOf(3.8051750381e-7));
			put("sec2yar", Double.valueOf(3.1709791984e-8));

			put("min2mls", Double.valueOf(60000));
			put("min2sec", Double.valueOf(60));
			put("min2min", Double.valueOf(1));
			put("min2hou", Double.valueOf(0.016666666667));
			put("min2day", Double.valueOf(0.00069444444444));
			put("min2wek", Double.valueOf(0.000099206349206));
			put("min2mon", Double.valueOf(0.000022831050228));
			put("min2yar", Double.valueOf(0.000001902587519));

			put("hou2mls", Double.valueOf(3600000));
			put("hou2sec", Double.valueOf(3600));
			put("hou2min", Double.valueOf(60));
			put("hou2hou", Double.valueOf(1));
			put("hou2day", Double.valueOf(0.041666666667));
			put("hou2wek", Double.valueOf(0.0059523809524));
			put("hou2mon", Double.valueOf(0.0013698630137));
			put("hou2yar", Double.valueOf(0.00011415525114));

			put("day2mls", Double.valueOf(86400000));
			put("day2sec", Double.valueOf(86400));
			put("day2min", Double.valueOf(1440));
			put("day2hou", Double.valueOf(24));
			put("day2day", Double.valueOf(1));
			put("day2wek", Double.valueOf(0.14285714286));
			put("day2mon", Double.valueOf(0.032876712329));
			put("day2yar", Double.valueOf(0.0027397260274));

			put("wek2mls", Double.valueOf(604800000));
			put("wek2sec", Double.valueOf(604800));
			put("wek2min", Double.valueOf(10080));
			put("wek2hou", Double.valueOf(168));
			put("wek2day", Double.valueOf(7));
			put("wek2wek", Double.valueOf(1));
			put("wek2mon", Double.valueOf(0.2301369863));
			put("wek2yar", Double.valueOf(0.019178082192));

			put("mon2mls", Double.valueOf(2.628E9));
			put("mon2sec", Double.valueOf(2628000));
			put("mon2min", Double.valueOf(43800));
			put("mon2hou", Double.valueOf(730));
			put("mon2day", Double.valueOf(30.416666667));
			put("mon2wek", Double.valueOf(4.3452380952));
			put("mon2mon", Double.valueOf(1));
			put("mon2yar", Double.valueOf(0.083333333333));

			put("yar2mls", Double.valueOf(3.1536E10));
			put("yar2sec", Double.valueOf(31536000));
			put("yar2min", Double.valueOf(525600));
			put("yar2hou", Double.valueOf(8760));
			put("yar2day", Double.valueOf(365));
			put("yar2wek", Double.valueOf(52.142857143));
			put("yar2mon", Double.valueOf(12));
			put("yar2yar", Double.valueOf(1));

			/**************************** setting value for volume ***************************/

			put("aft2aft", Double.valueOf(1));
			put("aft2ain", Double.valueOf(12));
			put("aft2cft", Double.valueOf(43560.000627));
			put("aft2cin", Double.valueOf(75271681.083));
			put("aft2cmt", Double.valueOf(1233.4818553));

			put("ain2aft", Double.valueOf(0.083333333335));
			put("ain2ain", Double.valueOf(1));
			put("ain2cft", Double.valueOf(3630.0000523));
			put("ain2cin", Double.valueOf(6272640.0904));
			put("ain2cmt", Double.valueOf(102.79015461));

			put("cft2aft", Double.valueOf(0.000022956840808));
			put("cft2ain", Double.valueOf(0.00027548208969));
			put("cft2cft", Double.valueOf(1));
			put("cft2cin", Double.valueOf(1728));
			put("cft2cmt", Double.valueOf(0.028316846592));

			put("cin2aft", Double.valueOf(1.3285208801e-8));
			put("cin2ain", Double.valueOf(1.5942250561e-7));
			put("cin2cft", Double.valueOf(0.0005787037037));
			put("cin2cin", Double.valueOf(1));
			put("cin2cmt", Double.valueOf(0.000016387064));

			put("cmt2aft", Double.valueOf(0.00081071318212));
			put("cmt2ain", Double.valueOf(0.0097285581853));
			put("cmt2cft", Double.valueOf(35.314666721));
			put("cmt2cin", Double.valueOf(61023.744095));
			put("cmt2cmt", Double.valueOf(1));

		}
	};

}
