package com.example.unitconverter;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity {

	// view object
	private Spinner conversionType, fromType, toType;
	private Button clearButton, swapButton, convertButton;
	private EditText fromEditText, toEditText;

	// data source
	private static String[] type = { "Angle", "Area", "BitsAndBytes",
			"Density", "Electric_Current", "Energy", "Force", "Length", "Mass",
			"Power", "Pressure", "Speed", "Temperature", "Time", "Volume" };

	/********************* data source for sub types **********************/

	private static String[] Angle = { "Radian", "Mil", "Grad", "Degree" };

	private static String[] Area = { "Acre", "Hectare", "Square",
			"Square_Foot", "Square_Inch", "Square_Kilometer", "Square_meter",
			"Square_mile" };
	private static String[] BitsAndBytes = { "Bit", "Byte", "Kilobyte",
			"Megabyte", "GigaByte" };
	private static String[] Density = { "Gram/Kiloliter", "Garm/Liter",
			"Garm/Microliter", "Garm/Mililiter", "Kilogram/Liter" };
	private static String[] Electric_Current = { "Ampere", "Gigaampere",
			"Kiloampere", "Miliampere", "Volt/Ohm", "Watt/Volt" };
	private static String[] Energy = { "Calorie", "Electronvolt", "Erg",
			"Gigajoule", "Gramcalorie", "Joule", "Kilocalorie", "Kilojoule",
			"Watthour" };
	private static String[] Force = { "Dyne", "Kilonewton", "Newton" };
	private static String[] Length = { "Centimeter", "Feet", "Inch",
			"Kilometer", "Meter", "Mile" };
	private static String[] Mass = { "Kilogram", "Ounce", "Pound", "Ton" };
	private static String[] Power = { "Gigawatt", "hectowatt", "Kilowatt",
			"Megawatt", "Watt" };
	private static String[] Pressure = { "Atmosphere", "Gigapascal",
			"Kilopascal", "Megapascal", "Pascal" };
	private static String[] Speed = { "Meter/Second", "Mile/Second",
			"Kilometer/Hour", "Kilometer/Second" };
	private static String[] Temperature = { "Degree_Celcius",
			"Degree_Farenhite", "Kelvin" };
	private static String[] Time = { "Milisecond", "Second", "Minute", "Hour",
			"Day", "Week", "Month", "Year" };
	private static String[] Volume = { "Acre_Foot", "Acre_Inch", "Cubic_Foot",
			"Cubic_Inch", "Cubic_Meter" };

	// adapter

	ArrayAdapter<String> adapter;
	ArrayAdapter<String> angleAdapter;
	ArrayAdapter<String> areaAdapter;
	ArrayAdapter<String> bitsandbytesAdapter;
	ArrayAdapter<String> densityAdapter;
	ArrayAdapter<String> electric_currentAdapter;
	ArrayAdapter<String> energyAdapter;
	ArrayAdapter<String> forceAdapter;
	ArrayAdapter<String> lengthAdapter;
	ArrayAdapter<String> massAdapter;
	ArrayAdapter<String> powerAdapter;
	ArrayAdapter<String> pressureAdapter;
	ArrayAdapter<String> speedAdapter;
	ArrayAdapter<String> temperatureAdapter;
	ArrayAdapter<String> timeAdapter;
	ArrayAdapter<String> volumeAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// initialize view
		conversionType = (Spinner) findViewById(R.id.selectConversionType);
		fromType = (Spinner) findViewById(R.id.selectFromType);
		toType = (Spinner) findViewById(R.id.ToType);
		clearButton = (Button) findViewById(R.id.clearButton);
		swapButton = (Button) findViewById(R.id.swapButton);
		convertButton = (Button) findViewById(R.id.convertButton);
		fromEditText = (EditText) findViewById(R.id.fromEditBox);
		toEditText = (EditText) findViewById(R.id.toEditBox);

		// initialize adapter
		adapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, type);
		angleAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Angle);

		areaAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Area);
		bitsandbytesAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, BitsAndBytes);
		densityAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Density);
		electric_currentAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Electric_Current);
		energyAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Energy);
		forceAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Force);
		lengthAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Length);
		massAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Mass);
		powerAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Power);
		pressureAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Pressure);
		speedAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Speed);
		temperatureAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Temperature);
		timeAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Time);
		volumeAdapter = new ArrayAdapter<String>(MainActivity.this,
				android.R.layout.simple_spinner_item, Volume);

		// bind adapter and view
		conversionType.setAdapter(adapter);

		conversionType.setOnItemSelectedListener(listener);

	}

	OnItemSelectedListener listener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View v, int position,
				long id) {

			String selectedItem = (String) conversionType.getSelectedItem();

			/************************ setting adapter for sub types ************************/

			if ("Angle".equals(selectedItem)) {
				fromType.setAdapter(angleAdapter);
				toType.setAdapter(angleAdapter);

			}
			if ("Area".equals(selectedItem)) {
				fromType.setAdapter(areaAdapter);
				toType.setAdapter(areaAdapter);

			}

			if ("BitsAndBytes".equals(selectedItem)) {
				fromType.setAdapter(bitsandbytesAdapter);
				toType.setAdapter(bitsandbytesAdapter);

			}
			if ("Density".equals(selectedItem)) {
				fromType.setAdapter(densityAdapter);
				toType.setAdapter(densityAdapter);

			}
			if ("Electric_Current".equals(selectedItem)) {
				fromType.setAdapter(electric_currentAdapter);
				toType.setAdapter(electric_currentAdapter);

			}
			if ("Energy".equals(selectedItem)) {
				fromType.setAdapter(energyAdapter);
				toType.setAdapter(energyAdapter);

			}
			if ("Force".equals(selectedItem)) {
				fromType.setAdapter(forceAdapter);
				toType.setAdapter(forceAdapter);

			}
			if ("Length".equals(selectedItem)) {
				fromType.setAdapter(lengthAdapter);
				toType.setAdapter(lengthAdapter);

			}
			if ("Mass".equals(selectedItem)) {
				fromType.setAdapter(massAdapter);
				toType.setAdapter(massAdapter);

			}
			if ("Power".equals(selectedItem)) {
				fromType.setAdapter(powerAdapter);
				toType.setAdapter(powerAdapter);

			}
			if ("Pressure".equals(selectedItem)) {
				fromType.setAdapter(pressureAdapter);
				toType.setAdapter(pressureAdapter);

			}
			if ("Speed".equals(selectedItem)) {
				fromType.setAdapter(speedAdapter);
				toType.setAdapter(speedAdapter);

			}
			if ("Temperature".equals(selectedItem)) {
				fromType.setAdapter(temperatureAdapter);
				toType.setAdapter(temperatureAdapter);

			}
			if ("Time".equals(selectedItem)) {
				fromType.setAdapter(timeAdapter);
				toType.setAdapter(timeAdapter);

			}
			if ("Volume".equals(selectedItem)) {
				fromType.setAdapter(volumeAdapter);
				toType.setAdapter(volumeAdapter);

			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

		}
	};

	/************************ Method for receiving values ****************************/
	public void convert(View v) {

		if (fromEditText.getText().toString().trim().length() <= 0) {
			Toast.makeText(getApplicationContext(),
					"Please Enter A Value In The Textbox", Toast.LENGTH_LONG)
					.show();

		} else {
			String fromString = fromEditText.getText().toString();
			final double fromValue = Double.parseDouble(fromString);
			final String fromSelectedItem = (String) fromType.getSelectedItem();
			String toSelectedItem = (String) toType.getSelectedItem();
			calculateResult(fromValue, fromSelectedItem, toSelectedItem);

		}

	}

	/************************ Method for Calculate Result ****************************/

	private void calculateResult(double fromValue, String fromSelectedItem,
			String toSelectedItem) {

		/************************* calculation for angle conversion ****************************/

		if ("Degree".equals(fromSelectedItem)) {
			if ("Radian".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("deg2rad"));
			}
			if ("Mil".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("deg2mil"));

			}
			if ("Grad".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("deg2grad"));

			}
			if ("Degree".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("deg2deg"));

			}

		}
		if ("Mil".equals(fromSelectedItem)) {
			if ("Radian".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2rad"));

			}
			if ("Mil".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2mil"));

			}
			if ("Grad".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2grad"));

			}
			if ("Degree".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2deg"));

			}

		}
		if ("Radian".equals(fromSelectedItem)) {
			if ("Radian".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("rad2rad"));

			}
			if ("Mil".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("rad2mil"));

			}
			if ("Grad".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("rad2grad"));

			}
			if ("Degree".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("rad2deg"));

			}

		}
		if ("Grad".equals(fromSelectedItem)) {
			if ("Radian".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("grad2rad"));

			}
			if ("Mil".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("grad2mil"));

			}
			if ("Grad".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("grad2grad"));

			}
			if ("Degree".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("grad2deg"));

			}

		}
		/************************* calculation for Area conversion ****************************/

		if ("Acre".equals(fromSelectedItem)) {
			if ("Acre".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("acr2acr"));

			}
			if ("Hectare".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("acr2hec"));

			}
			if ("Square".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("acr2squ"));

			}
			if ("Square_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("acr2sqf"));

			}
			if ("Square_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("acr2sqi"));

			}
			if ("Square_Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("acr2sqk"));

			}
			if ("Square_meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("acr2sqm"));

			}
			if ("Square_mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("acr2sqml"));

			}

		}

		if ("Hectare".equals(fromSelectedItem)) {
			if ("Acre".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hec2acr"));

			}
			if ("Hectare".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hec2hec"));

			}
			if ("Square".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hec2squ"));

			}
			if ("Square_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hec2sqf"));

			}
			if ("Square_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hec2sqi"));

			}
			if ("Square_Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hec2sqk"));

			}
			if ("Square_meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hec2sqm"));

			}
			if ("Square_mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hec2sqml"));

			}

		}

		if ("Square".equals(fromSelectedItem)) {
			if ("Acre".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("squ2acr"));

			}
			if ("Hectare".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("squ2hec"));

			}
			if ("Square".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("squ2squ"));

			}
			if ("Square_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("squ2sqf"));

			}
			if ("Square_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("squ2sqi"));

			}
			if ("Square_Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("squ2sqk"));

			}
			if ("Square_meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("squ2sqm"));

			}
			if ("Square_mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("squ2sqml"));

			}

		}

		if ("Square_Foot".equals(fromSelectedItem)) {
			if ("Acre".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqf2acr"));

			}
			if ("Hectare".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqf2hec"));

			}
			if ("Square".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqf2squ"));

			}
			if ("Square_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqf2sqf"));

			}
			if ("Square_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqf2sqi"));

			}
			if ("Square_Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqf2sqk"));

			}
			if ("Square_meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqf2sqm"));

			}
			if ("Square_mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqf2sqml"));

			}

		}

		if ("Square_Inch".equals(fromSelectedItem)) {
			if ("Acre".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqi2acr"));

			}
			if ("Hectare".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqi2hec"));

			}
			if ("Square".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqi2squ"));

			}
			if ("Square_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqi2sqf"));

			}
			if ("Square_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqi2sqi"));

			}
			if ("Square_Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqi2sqk"));

			}
			if ("Square_meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqi2sqm"));

			}
			if ("Square_mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqi2sqml"));

			}

		}

		if ("Square_Kilometer".equals(fromSelectedItem)) {
			if ("Acre".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqk2acr"));

			}
			if ("Hectare".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqk2hec"));

			}
			if ("Square".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqk2squ"));

			}
			if ("Square_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqk2sqf"));

			}
			if ("Square_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqk2sqi"));

			}
			if ("Square_Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqk2sqk"));

			}
			if ("Square_meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqk2sqm"));

			}
			if ("Square_mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqk2sqml"));

			}

		}

		if ("Square_meter".equals(fromSelectedItem)) {
			if ("Acre".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqm2acr"));

			}
			if ("Hectare".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqm2hec"));

			}
			if ("Square".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqm2squ"));

			}
			if ("Square_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqm2sqf"));

			}
			if ("Square_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqm2sqi"));

			}
			if ("Square_Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqm2sqk"));

			}
			if ("Square_meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqm2sqm"));

			}
			if ("Square_mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqm2sqml"));

			}

		}

		if ("Square_mile".equals(fromSelectedItem)) {
			if ("Acre".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqml2acr"));

			}
			if ("Hectare".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqml2hec"));

			}
			if ("Square".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqml2squ"));

			}
			if ("Square_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqml2sqf"));

			}
			if ("Square_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqml2sqi"));

			}
			if ("Square_Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqml2sqk"));

			}
			if ("Square_meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqml2sqm"));

			}
			if ("Square_mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sqml2sqml"));

			}

		}

		/************************* calculation for Bits And Bytes conversion ****************************/

		if ("Bit".equals(fromSelectedItem)) {
			if ("Bit".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("bit2bit"));

			}
			if ("Byte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("bit2byt"));

			}
			if ("Kilobyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("bit2kbt"));

			}
			if ("Megabyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("bit2mbt"));

			}
			if ("GigaByte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("bit2gbt"));

			}

		}

		if ("Byte".equals(fromSelectedItem)) {
			if ("Bit".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("byt2bit"));

			}
			if ("Byte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("byt2byt"));

			}
			if ("Kilobyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("byt2kbt"));

			}
			if ("Megabyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("byt2mbt"));

			}
			if ("GigaByte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("byt2gbt"));

			}

		}

		if ("Kilobyte".equals(fromSelectedItem)) {
			if ("Bit".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kbt2bit"));

			}
			if ("Byte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kbt2byt"));

			}
			if ("Kilobyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kbt2kbt"));

			}
			if ("Megabyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kbt2mbt"));

			}
			if ("GigaByte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kbt2gbt"));

			}

		}

		if ("Megabyte".equals(fromSelectedItem)) {
			if ("Bit".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mbt2bit"));

			}
			if ("Byte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mbt2byt"));

			}
			if ("Kilobyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mbt2kbt"));

			}
			if ("Megabyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mbt2mbt"));

			}
			if ("GigaByte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mbt2gbt"));

			}

		}

		if ("GigaByte".equals(fromSelectedItem)) {
			if ("Bit".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gbt2bit"));

			}
			if ("Byte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gbt2byt"));

			}
			if ("Kilobyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gbt2kbt"));

			}
			if ("Megabyte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gbt2mbt"));

			}
			if ("GigaByte".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gbt2gbt"));

			}

		}
		/************************* calculation for Density conversion ****************************/

		if ("Gram/Kiloliter".equals(fromSelectedItem)) {
			if ("Gram/Kiloliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gkl2gkl"));

			}
			if ("Garm/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gkl2glt"));

			}
			if ("Garm/Microliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gkl2gmrl"));

			}
			if ("Garm/Mililiter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gkl2gml"));

			}
			if ("Kilogram/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gkl2klt"));

			}

		}

		if ("Garm/Liter".equals(fromSelectedItem)) {
			if ("Gram/Kiloliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("glt2gkl"));

			}
			if ("Garm/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("glt2glt"));

			}
			if ("Garm/Microliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("glt2gmrl"));

			}
			if ("Garm/Mililiter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("glt2gml"));

			}
			if ("Kilogram/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("glt2klt"));

			}

		}

		if ("Garm/Microliter".equals(fromSelectedItem)) {
			if ("Gram/Kiloliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmrl2gkl"));

			}
			if ("Garm/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmrl2glt"));

			}
			if ("Garm/Microliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmrl2gmrl"));

			}
			if ("Garm/Mililiter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmrl2gml"));

			}
			if ("Kilogram/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmrl2klt"));

			}

		}

		if ("Garm/Mililiter".equals(fromSelectedItem)) {
			if ("Gram/Kiloliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gml2gkl"));

			}
			if ("Garm/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gml2glt"));

			}
			if ("Garm/Microliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gml2gmrl"));

			}
			if ("Garm/Mililiter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gml2gml"));

			}
			if ("Kilogram/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gml2klt"));

			}

		}

		if ("Kilogram/Liter".equals(fromSelectedItem)) {
			if ("Gram/Kiloliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("klt2gkl"));

			}
			if ("Garm/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("klt2glt"));

			}
			if ("Garm/Microliter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("klt2gmrl"));

			}
			if ("Garm/Mililiter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("klt2gml"));

			}
			if ("Kilogram/Liter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("klt2klt"));

			}

		}

		/************************* calculation for electric current conversion ****************************/

		if ("Ampere".equals(fromSelectedItem)) {
			if ("Ampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("amp2amp"));

			}
			if ("Gigaampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("amp2gmp"));

			}
			if ("Kiloampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("amp2kmp"));

			}
			if ("Miliampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("amp2mmp"));

			}
			if ("Volt/Ohm".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("amp2voh"));

			}
			if ("Watt/Volt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("amp2wvt"));

			}

		}

		if ("Gigaampere".equals(fromSelectedItem)) {
			if ("Ampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmp2amp"));

			}
			if ("Gigaampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmp2gmp"));

			}
			if ("Kiloampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmp2kmp"));

			}
			if ("Miliampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmp2mmp"));

			}
			if ("Volt/Ohm".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmp2voh"));

			}
			if ("Watt/Volt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gmp2wvt"));

			}

		}

		if ("Kiloampere".equals(fromSelectedItem)) {
			if ("Ampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmp2amp"));

			}
			if ("Gigaampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmp2gmp"));

			}
			if ("Kiloampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmp2kmp"));

			}
			if ("Miliampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmp2mmp"));

			}
			if ("Volt/Ohm".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmp2voh"));

			}
			if ("Watt/Volt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmp2wvt"));

			}

		}

		if ("Miliampere".equals(fromSelectedItem)) {
			if ("Ampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mmp2amp"));

			}
			if ("Gigaampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mmp2gmp"));

			}
			if ("Kiloampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mmp2kmp"));

			}
			if ("Miliampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mmp2mmp"));

			}
			if ("Volt/Ohm".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mmp2voh"));

			}
			if ("Watt/Volt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mmp2wvt"));

			}

		}

		if ("Volt/Ohm".equals(fromSelectedItem)) {
			if ("Ampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("voh2amp"));

			}
			if ("Gigaampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("voh2gmp"));

			}
			if ("Kiloampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("voh2kmp"));

			}
			if ("Miliampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("voh2mmp"));

			}
			if ("Volt/Ohm".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("voh2voh"));

			}
			if ("Watt/Volt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("voh2wvt"));

			}

		}

		if ("Watt/Volt".equals(fromSelectedItem)) {
			if ("Ampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wvt2amp"));

			}
			if ("Gigaampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wvt2gmp"));

			}
			if ("Kiloampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wvt2kmp"));

			}
			if ("Miliampere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wvt2mmp"));

			}
			if ("Volt/Ohm".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wvt2voh"));

			}
			if ("Watt/Volt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wvt2wvt"));

			}

		}
		/************************* calculation for energy conversion ****************************/

		if ("Calorie".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cal2whr"));

			}

		}

		if ("Electronvolt".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("evt2whr"));

			}

		}

		if ("Erg".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("erg2whr"));

			}

		}

		if ("Gigajoule".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gjl2whr"));

			}

		}

		if ("Gramcalorie".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gcl2whr"));

			}

		}

		if ("Joule".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("jul2whr"));

			}

		}

		if ("Kilocalorie".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kcl2whr"));

			}

		}

		if ("Kilojoule".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kjl2whr"));

			}

		}

		if ("Watthour".equals(fromSelectedItem)) {
			if ("Calorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2cal"));

			}
			if ("Electronvolt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2evt"));

			}
			if ("Erg".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2erg"));

			}
			if ("Gigajoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2gjl"));

			}
			if ("Gramcalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2gcl"));

			}
			if ("Joule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2jul"));

			}
			if ("Kilocalorie".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2kcl"));

			}
			if ("Kilojoule".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2kjl"));

			}
			if ("Watthour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("whr2whr"));

			}

		}
		/************************* calculation for Force conversion ****************************/

		if ("Dyne".equals(fromSelectedItem)) {
			if ("Dyne".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dyn2dyn"));

			}
			if ("Kilonewton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dyn2knt"));

			}
			if ("Newton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dyn2new"));

			}
		}

		if ("Kilonewton".equals(fromSelectedItem)) {
			if ("Dyne".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("knt2dyn"));

			}
			if ("Kilonewton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("knt2knt"));

			}
			if ("Newton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("knt2new"));

			}
		}

		if ("Newton".equals(fromSelectedItem)) {
			if ("Dyne".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("new2dyn"));

			}
			if ("Kilonewton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("new2knt"));

			}
			if ("Newton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("new2new"));

			}
		}
		/************************* calculation for Length conversion ****************************/

		if ("Centimeter".equals(fromSelectedItem)) {
			if ("Centimeter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2cmt"));

			}
			if ("Feet".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2fet"));

			}
			if ("Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2inc"));

			}
			if ("Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2kmt"));

			}
			if ("Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2mtr"));

			}
			if ("Mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2mil"));

			}
		}

		if ("Feet".equals(fromSelectedItem)) {
			if ("Centimeter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("fet2cmt"));

			}
			if ("Feet".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("fet2fet"));

			}
			if ("Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("fet2inc"));

			}
			if ("Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("fet2kmt"));

			}
			if ("Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("fet2mtr"));

			}
			if ("Mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("fet2mil"));

			}
		}

		if ("Inch".equals(fromSelectedItem)) {
			if ("Centimeter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("inc2cmt"));

			}
			if ("Feet".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("inc2fet"));

			}
			if ("Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("inc2inc"));

			}
			if ("Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("inc2kmt"));

			}
			if ("Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("inc2mtr"));

			}
			if ("Mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("inc2mil"));

			}
		}

		if ("Kilometer".equals(fromSelectedItem)) {
			if ("Centimeter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmt2cmt"));

			}
			if ("Feet".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmt2fet"));

			}
			if ("Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmt2inc"));

			}
			if ("Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmt2kmt"));

			}
			if ("Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmt2mtr"));

			}
			if ("Mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kmt2mil"));

			}
		}

		if ("Meter".equals(fromSelectedItem)) {
			if ("Centimeter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mtr2cmt"));

			}
			if ("Feet".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mtr2fet"));

			}
			if ("Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mtr2inc"));

			}
			if ("Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mtr2kmt"));

			}
			if ("Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mtr2mtr"));

			}
			if ("Mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mtr2mil"));

			}
		}

		if ("Mile".equals(fromSelectedItem)) {
			if ("Centimeter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2cmt"));

			}
			if ("Feet".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2fet"));

			}
			if ("Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2inc"));

			}
			if ("Kilometer".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2kmt"));

			}
			if ("Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2mtr"));

			}
			if ("Mile".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mil2mil"));

			}
		}
		/************************* calculation for Mass conversion ****************************/

		if ("Kilogram".equals(fromSelectedItem)) {
			if ("Kilogram".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kgm2kgm"));

			}
			if ("Ounce".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kgm2oun"));

			}
			if ("Pound".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kgm2pou"));

			}
			if ("Ton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kgm2ton"));

			}
		}

		if ("Ounce".equals(fromSelectedItem)) {
			if ("Kilogram".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("oun2kgm"));

			}
			if ("Ounce".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("oun2oun"));

			}
			if ("Pound".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("oun2pou"));

			}
			if ("Ton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("oun2ton"));

			}
		}

		if ("Pound".equals(fromSelectedItem)) {
			if ("Kilogram".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pou2kgm"));

			}
			if ("Ounce".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pou2oun"));

			}
			if ("Pound".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pou2pou"));

			}
			if ("Ton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pou2ton"));

			}
		}

		if ("Ton".equals(fromSelectedItem)) {
			if ("Kilogram".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ton2kgm"));

			}
			if ("Ounce".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ton2oun"));

			}
			if ("Pound".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ton2pou"));

			}
			if ("Ton".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ton2ton"));

			}
		}
		/************************* calculation for Power conversion ****************************/

		if ("Gigawatt".equals(fromSelectedItem)) {
			if ("Gigawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gwt2gwt"));

			}
			if ("hectowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gwt2hwt"));

			}
			if ("Kilowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gwt2kwt"));

			}
			if ("Megawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gwt2mwt"));

			}
			if ("Watt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gwt2wat"));

			}
		}

		if ("hectowatt".equals(fromSelectedItem)) {
			if ("Gigawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hwt2gwt"));

			}
			if ("hectowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hwt2hwt"));

			}
			if ("Kilowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hwt2kwt"));

			}
			if ("Megawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hwt2mwt"));

			}
			if ("Watt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hwt2wat"));

			}
		}

		if ("Kilowatt".equals(fromSelectedItem)) {
			if ("Gigawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kwt2gwt"));

			}
			if ("hectowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kwt2hwt"));

			}
			if ("Kilowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kwt2kwt"));

			}
			if ("Megawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kwt2mwt"));

			}
			if ("Watt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kwt2wat"));

			}
		}

		if ("Megawatt".equals(fromSelectedItem)) {
			if ("Gigawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mwt2gwt"));

			}
			if ("hectowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mwt2hwt"));

			}
			if ("Kilowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mwt2kwt"));

			}
			if ("Megawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mwt2mwt"));

			}
			if ("Watt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mwt2wat"));

			}
		}

		if ("Watt".equals(fromSelectedItem)) {
			if ("Gigawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wat2gwt"));

			}
			if ("hectowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wat2hwt"));

			}
			if ("Kilowatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wat2kwt"));

			}
			if ("Megawatt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wat2mwt"));

			}
			if ("Watt".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wat2wat"));

			}
		}
		/************************* calculation for Pressure conversion ****************************/

		if ("Atmosphere".equals(fromSelectedItem)) {
			if ("Atmosphere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("atm2atm"));

			}
			if ("Gigapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("atm2gps"));

			}
			if ("Kilopascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("atm2kps"));

			}
			if ("Megapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("atm2mps"));

			}
			if ("Pascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("atm2pas"));

			}
		}

		if ("Gigapascal".equals(fromSelectedItem)) {
			if ("Atmosphere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gps2atm"));

			}
			if ("Gigapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gps2gps"));

			}
			if ("Kilopascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gps2kps"));

			}
			if ("Megapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gps2mps"));

			}
			if ("Pascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("gps2pas"));

			}
		}

		if ("Kilopascal".equals(fromSelectedItem)) {
			if ("Atmosphere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2atm"));

			}
			if ("Gigapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2gps"));

			}
			if ("Kilopascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2kps"));

			}
			if ("Megapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2mps"));

			}
			if ("Pascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2pas"));

			}
		}

		if ("Megapascal".equals(fromSelectedItem)) {
			if ("Atmosphere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2atm"));

			}
			if ("Gigapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2gps"));

			}
			if ("Kilopascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2kps"));

			}
			if ("Megapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2mps"));

			}
			if ("Pascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2pas"));

			}
		}

		if ("Pascal".equals(fromSelectedItem)) {
			if ("Atmosphere".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pas2atm"));

			}
			if ("Gigapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pas2gps"));

			}
			if ("Kilopascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pas2kps"));

			}
			if ("Megapascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pas2mps"));

			}
			if ("Pascal".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("pas2pas"));

			}
		}

		/************************* calculation for Speed conversion ****************************/

		if ("Meter/Second".equals(fromSelectedItem)) {
			if ("Meter/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2mps"));

			}
			if ("Mile/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2mls"));

			}
			if ("Kilometer/Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2kph"));

			}
			if ("Kilometer/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mps2kps"));

			}

		}

		if ("Mile/Second".equals(fromSelectedItem)) {
			if ("Meter/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2mps"));

			}
			if ("Mile/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2mls"));

			}
			if ("Kilometer/Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2kph"));

			}
			if ("Kilometer/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2kps"));

			}

		}

		if ("Kilometer/Hour".equals(fromSelectedItem)) {
			if ("Meter/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kph2mps"));

			}
			if ("Mile/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kph2mls"));

			}
			if ("Kilometer/Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kph2kph"));

			}
			if ("Kilometer/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kph2kps"));

			}

		}

		if ("Kilometer/Second".equals(fromSelectedItem)) {
			if ("Meter/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2mps"));

			}
			if ("Mile/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2mls"));

			}
			if ("Kilometer/Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2kph"));

			}
			if ("Kilometer/Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kps2kps"));

			}

		}
		/************************* calculation for Temperature conversion ****************************/

		if ("Degree_Celcius".equals(fromSelectedItem)) {
			if ("Degree_Celcius".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dcs2dcs"));

			}
			if ("Degree_Farenhite".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dcs2dfr"));

			}
			if ("Kelvin".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dcs2kvn"));

			}

		}

		if ("Degree_Farenhite".equals(fromSelectedItem)) {
			if ("Degree_Celcius".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dfr2dcs"));

			}
			if ("Degree_Farenhite".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dfr2dfr"));

			}
			if ("Kelvin".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("dfr2kvn"));

			}

		}

		if ("Kelvin".equals(fromSelectedItem)) {
			if ("Degree_Celcius".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kvn2dcs"));

			}
			if ("Degree_Farenhite".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kvn2dfr"));

			}
			if ("Kelvin".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("kvn2kvn"));

			}

		}
		/************************* calculation for Time conversion ****************************/

		if ("Milisecond".equals(fromSelectedItem)) {
			if ("Milisecond".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2mls"));

			}
			if ("Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2sec"));

			}
			if ("Minute".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2min"));

			}
			if ("Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2hou"));

			}
			if ("Day".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2day"));

			}
			if ("Week".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2wek"));

			}
			if ("Month".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2mon"));

			}
			if ("Year".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mls2yar"));

			}

		}

		if ("Second".equals(fromSelectedItem)) {
			if ("Milisecond".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sec2mls"));

			}
			if ("Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sec2sec"));

			}
			if ("Minute".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sec2min"));

			}
			if ("Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sec2hou"));

			}
			if ("Day".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sec2day"));

			}
			if ("Week".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sec2wek"));

			}
			if ("Month".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sec2mon"));

			}
			if ("Year".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("sec2yar"));

			}

		}

		if ("Minute".equals(fromSelectedItem)) {
			if ("Milisecond".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("min2mls"));

			}
			if ("Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("min2sec"));

			}
			if ("Minute".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("min2min"));

			}
			if ("Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("min2hou"));

			}
			if ("Day".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("min2day"));

			}
			if ("Week".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("min2wek"));

			}
			if ("Month".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("min2mon"));

			}
			if ("Year".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("min2yar"));

			}

		}

		if ("Hour".equals(fromSelectedItem)) {
			if ("Milisecond".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hou2mls"));

			}
			if ("Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hou2sec"));

			}
			if ("Minute".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hou2min"));

			}
			if ("Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hou2hou"));

			}
			if ("Day".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hou2day"));

			}
			if ("Week".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hou2wek"));

			}
			if ("Month".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hou2mon"));

			}
			if ("Year".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("hou2yar"));

			}

		}

		if ("Day".equals(fromSelectedItem)) {
			if ("Milisecond".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("day2mls"));

			}
			if ("Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("day2sec"));

			}
			if ("Minute".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("day2min"));

			}
			if ("Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("day2hou"));

			}
			if ("Day".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("day2day"));

			}
			if ("Week".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("day2wek"));

			}
			if ("Month".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("day2mon"));

			}
			if ("Year".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("day2yar"));

			}

		}

		if ("Week".equals(fromSelectedItem)) {
			if ("Milisecond".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wek2mls"));

			}
			if ("Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wek2sec"));

			}
			if ("Minute".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wek2min"));

			}
			if ("Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wek2hou"));

			}
			if ("Day".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wek2day"));

			}
			if ("Week".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wek2wek"));

			}
			if ("Month".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wek2mon"));

			}
			if ("Year".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("wek2yar"));

			}

		}

		if ("Month".equals(fromSelectedItem)) {
			if ("Milisecond".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mon2mls"));

			}
			if ("Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mon2sec"));

			}
			if ("Minute".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mon2min"));

			}
			if ("Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mon2hou"));

			}
			if ("Day".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mon2day"));

			}
			if ("Week".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mon2wek"));

			}
			if ("Month".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mon2mon"));

			}
			if ("Year".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("mon2yar"));

			}

		}

		if ("Year".equals(fromSelectedItem)) {
			if ("Milisecond".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("yar2mls"));

			}
			if ("Second".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("yar2sec"));

			}
			if ("Minute".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("yar2min"));

			}
			if ("Hour".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("yar2hou"));

			}
			if ("Day".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("yar2day"));

			}
			if ("Week".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("yar2wek"));

			}
			if ("Month".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("yar2mon"));

			}
			if ("Year".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("yar2yar"));

			}

		}
		/************************* calculation for Volume conversion ****************************/

		if ("Acre_Foot".equals(fromSelectedItem)) {
			if ("Acre_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("aft2aft"));

			}
			if ("Acre_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("aft2ain"));

			}
			if ("Cubic_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("aft2cft"));

			}
			if ("Cubic_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("aft2cin"));

			}
			if ("Cubic_Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("aft2cmt"));

			}

		}

		if ("Acre_Inch".equals(fromSelectedItem)) {
			if ("Acre_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ain2aft"));

			}
			if ("Acre_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ain2ain"));

			}
			if ("Cubic_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ain2cft"));

			}
			if ("Cubic_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ain2cin"));

			}
			if ("Cubic_Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("ain2cmt"));

			}

		}

		if ("Cubic_Foot".equals(fromSelectedItem)) {
			if ("Acre_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cft2aft"));

			}
			if ("Acre_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cft2ain"));

			}
			if ("Cubic_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cft2cft"));

			}
			if ("Cubic_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cft2cin"));

			}
			if ("Cubic_Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cft2cmt"));

			}

		}

		if ("Cubic_Inch".equals(fromSelectedItem)) {
			if ("Acre_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cin2aft"));

			}
			if ("Acre_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cin2ain"));

			}
			if ("Cubic_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cin2cft"));

			}
			if ("Cubic_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cin2cin"));

			}
			if ("Cubic_Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cin2cmt"));

			}

		}

		if ("Cubic_Meter".equals(fromSelectedItem)) {
			if ("Acre_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2aft"));

			}
			if ("Acre_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2ain"));

			}
			if ("Cubic_Foot".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2cft"));

			}
			if ("Cubic_Inch".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2cin"));

			}
			if ("Cubic_Meter".equals(toSelectedItem)) {
				setResultOnEditText(fromValue
						* UnitConversionValues.unitValuesMap.get("cmt2cmt"));

			}

		}

	}

	private void setResultOnEditText(double finalResult) {
		final String stringdouble = Double.toString(finalResult);
		toEditText.setText(stringdouble);
	}

	public void clear(View v) {
		fromEditText.setText("");
		toEditText.setText("");
		conversionType.setSelection(0);
		fromType.setSelection(0);
		toType.setSelection(0);

	}

	public void swap(View v) {
		int toTypePos = toType.getSelectedItemPosition();
		int fromTypePos = fromType.getSelectedItemPosition();
		if (fromType != null)
			fromType.setSelection(toTypePos, true);
		if (toType != null)
			toType.setSelection(fromTypePos, true);

		String fromString = fromEditText.getText().toString();
		String toString = toEditText.getText().toString();
		fromEditText.setText(toString);
		toEditText.setText(fromString);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
